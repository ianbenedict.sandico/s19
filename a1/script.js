// let movie = [
// 	{
// 		title: "Jaws",
// 		genre: "Thriller",
// 		releaseDate: "8/27/2021",
// 		rating: 6.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	},
// 	{
// 		title: "Spider-Man",
// 		genre: "Action",
// 		releaseDate: "7/23/2020",
// 		rating: 8.0,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	},
// 	{
// 		title: "Lord of the ring",
// 		genre: "Adventure",
// 		releaseDate: "3/6/2000",
// 		rating: 8.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	},
// 	{
// 		title: "Crazy Grandpa",
// 		genre: "Comedy",
// 		releaseDate: "2/5/2015",
// 		rating: 9.5,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	},
// 	{
// 		title: "Gagamboy",
// 		genre: "Drama",
// 		releaseDate: "3/15/1996",
// 		rating: 2,
// 		displayRating: function(){
// 			return "The movie " + this.title +" has " + this.rating + " stars."
// 		}
// 	}
// 	]

// const showAllMovies =() =>{
// 	for (let i = 0; i < movie.length; i++){
// 		if (movie[i].genre == "Thriller" || movie[i].genre == "Comedy" || movie[i].genre == "Drama"){
// 			console.log(movie[i].title + " is a " + movie[i].genre + " movie ")
// 		}else{
// 			console.log(movie[i].title + " is an " + movie[i].genre + " movie ");
// 		}
// 	}
// }

// showAllMovies()





let movies = [
{
	title: "Exorcist",
	stars: 4,
	displayRating: function(){
		return this.stars
	}
},
{
	title: "Godfather",
	stars: 5,
	displayRating: function(){
		return  this.stars
	}
}
]

const showTitle =(stars) => {
	let ratingArr = [];

	for(let i = 0; i < movies.length; i++){
		ratingArr.push(movies[i].stars)
	}
	
	for(let x = 0; x <ratingArr.length; x++){
		if(ratingArr.includes(stars)){
			if(movies[x].stars >= ratingArr[x]){
				console.log(`${movies[x].title} ${movies[x].stars} stars`);
			}
		}else{
			console.log("No Movie with that rating")
		}
	}
}

showTitle(4)
showTitle(6)