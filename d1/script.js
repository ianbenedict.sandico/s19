
/*
//Primitive Data Types
1) Strings
2) Numbers
3) Boolean
4) Null
5) Undefined

*/

/*
Reference Types
-talk about address or reference, not value
- you can update arrays even if its constant, bec above
-let, pwedeng tabunan ang address, const hindi.

arrays
objects
*/

/*
Math Object


*/

/*
Inherent True or False Values
0 - false
'' - false
null- false
undefined - false

negative and positve numbers - true
string - true
*/

// let num = 23
// let loggedInUser = null;
// loggedInUser = 'Homer Simpson'

// if(loggedInUser	){
// 	console.log('The Value is true');
// }else{
// 	console.log('The Value is false');
// }

/*
Function Declaration versus Function Expression
2 + 3 = 5 //Expression

Hoisting
*/

// sayHello2();

// let result = sayHello() // Storing the fuction itself, refering to result of the function sayHello

// function sayHello(){      //function declaration. hoisting- it will read all function first. in function decleration, even if the invocation is first or last, it will still apply
// 	console.log('Hello')
// }

// // sayHello();

// const sayHello2 = function (){   //function expression,  the expression must be first. Storing the result of the function
// 	return('Hello')
// }


// sayHello2();

// function sayItAgain(func , num){
// 	for(let i = 0; i < num; i++){
// 		func()
// 	}
// }

// sayItAgain(sayHello2, 5)

/*
Template literal

*/

// let item = 'iPad';
// let price = 8000;
// let qty = 5;

// console.log('Total for ' + qty + '  ' + item + ' is ' + price * qty) 

// console.log(`Total for ${qty} ${item} is ${price * qty}`)  //template literal



/* Arrow Functions */
// remove function, add let and = to the variable
// next is => to the return statement, and remove return, curly braces.

// function addNums(num1, num2){
// 	return num1 + num2
// }

let addNums = (num1, num2) =>  num1 + num2

// function testNum(num){
// 	return num >= 0
// }

let testNum = num => num >= 0

// function sampleNum(){
// 	return Math.floor(Math.random()*10)
// }

let sampleNum = () => Math.floor(Math.random()*10)

/*
Array Destructuring

const a = vowerls[0]   //index position

left side store in const	=	right side the one to be destructed       //destructuring

*/

const vowels = ['a', 'e', 'i', 'o', 'u']

// const a = vowels[0]
// const e = vowels[1]



const [a, e, i, o, u] = vowels;
console.log(a);
console.log(e);
console.log(u);